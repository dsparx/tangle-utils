import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:spectoda_utils/src/tangle_parser/tangle_metadata_parser.dart';

void main() {
  final TangleMetadataParser parser = TangleMetadataParser();

  void _testTimelineBytes(String description, {
    required int clock,
    required int timeline,
    required bool isTimelinePaused,
    required List<int> output,
  }) {
    test(description, () {
      final Uint8List bytes = parser.timeBytes(
        clock: clock,
        timeline: timeline,
        isTimelinePaused: isTimelinePaused,
      );
      expect(bytes, output);
    });
  }

  void _testPacketBytes(String description, {
    required int uuid,
    required int from,
    required int to,
    required Uint8List payload,
    required List<int> output,
  }) {
    test(description, () {
      final Uint8List bytes = parser.packetBytes(
          uuid: uuid,
          from: from,
          to: to,
          payload: payload,
      );
      expect(bytes, output);
    });
  }

  group('TangleMetadataParser', () {
    group('packetBytes', () {
      _testPacketBytes('from/to',
        uuid: 57,
        from: 2,
        to: 6,
        payload: Uint8List.fromList([0, 1, 2, 3, 4, 5, 6, 7]),
        output: [
          57, 0, 0, 0,
          2, 0, 0, 0,
          8, 0, 0, 0,
          2, 3, 4, 5
        ],
      );
    });

    group('timeBytes', () {
      _testTimelineBytes('timeline not paused',
        clock: 123,
        timeline: 256,
        isTimelinePaused: false,
        output: [
          249,
          123, 0, 0, 0,
          0, 1, 0, 0,
          0,
        ],
      );

      _testTimelineBytes('timeline paused',
        clock: 123,
        timeline: 256,
        isTimelinePaused: true,
        output: [
          249,
          123, 0, 0, 0,
          0, 1, 0, 0,
          16,
        ],
      );
    });

    group('colorEventBytes', () {
      test('no deviceID', () {
        final Uint8List bytes = parser.colorEventBytes(
          label: '\$evt1',
          color: const Color.fromRGBO(255, 0, 0, 1),
          timeline: 259,
        );
        expect(bytes, [
          251,
          255, 0, 0,
          101, 118, 116, 49, 0,
          3, 1, 0, 0,
          255,
        ]);
      });
    });

    group('percentageEventBytes', () {
      test('', () {
        final Uint8List bytes = parser.percentageEventBytes(
          label: '\$evt1',
          percentage: 0.5,
          timeline: 259,
        );
        expect(bytes, [
          252,
          255, 255, 255, 63,
          101, 118, 116, 49, 0,
          3, 1, 0, 0,
          255
        ]);
      });
    });

    test('adoptRequest', () {
      final Uint8List bytes = parser.adoptRequestBytes(
        uuid: 123,
        ownerSignature: Uint8List.fromList(List.generate(16, (i) => i)),
        ownerKey: Uint8List.fromList(List.generate(16, (i) => 10*i)),
        deviceName: 'ABC',
        deviceID: 7,
      );
      expect(bytes, [
        240,
        123, 0, 0, 0,
        0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
        0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150,
        65, 66, 67, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        7
      ]);
    });

    test('network', () {
      final Uint8List bytes = parser.networkBytes(
        bytes: Uint8List.fromList([0, 1, 2, 3, 4]),
      );
      expect(bytes, [
        240,
        5, 0, 0, 0,
        0, 1, 2, 3, 4,
      ]);
    });

    group('ota', () {
      test('Reset', () {
        final Uint8List bytes = parser.otaReset();
        expect(bytes, [
          253,
          0,
          0, 0, 0, 0
        ]);
      });

      test('Begin', () {
        final Uint8List bytes = parser.otaBegin(
          firmware: Uint8List.fromList([0, 1, 2, 3]),
        );
        expect(bytes, [
          255,
          0,
          4, 0, 0, 0
        ]);
      });

      test('Write', () {
        final Uint8List bytes = parser.otaWrite(
          firmware: Uint8List.fromList([0, 1, 2, 3, 4, 5]),
          from: 1,
          to: 4,
          written: 3,
        );
        expect(bytes, [
          0,
          0,
          3, 0, 0, 0,
          1, 2, 3
        ]);
      });

      test('End', () {
        final Uint8List bytes = parser.otaEnd(
          written: 3,
        );
        expect(bytes, [
          254,
          0,
          3, 0, 0, 0,
        ]);
      });
    });

    test('Reboot', () {
      final Uint8List bytes = parser.rebootBytes();
      expect(bytes, [
        240,
        1, 0, 0, 0,
        5,
      ]);
    });
  });
}
