import 'package:flutter_test/flutter_test.dart';
import 'package:spectoda_utils/src/tangle_parser/enums/token_type.dart';
import 'package:spectoda_utils/src/tangle_parser/tokenizer/tangle_token.dart';
import 'package:spectoda_utils/src/tangle_parser/tokenizer/tangle_tokenizer.dart';

void main() {
  final TangleTokenizer tokenizer = TangleTokenizer();

  void _test(String description, {
    required String tangleCode,
    required Iterable<TangleToken> tokens,
  }) {
    test(description, () {
      final tangleTokens = tokenizer.tokenize(tangleCode);

      expect(tangleTokens.length, tokens.length);

      for (int i = 0; i < tokens.length; ++i) {
        expect(
            tangleTokens.elementAt(i).token,
            tokens.elementAt(i).token,
        );
        expect(
          tangleTokens.elementAt(i).type,
          tokens.elementAt(i).type,
        );
      }
    });
  }

  void _testIsolated(String description, {
    required String token,
    required TokenType type,
  }) {
    test(description, () {
      final tangleTokens = tokenizer.tokenize(token);

      expect(tangleTokens.length, 1);

      final tangleToken = tangleTokens.first;

      expect(tangleToken.token, token);
      expect(tangleToken.type, type);
    });
  }

  group('Tokenizer', () {
    group('simple', () {
      _test('empty',
        tangleCode: '',
        tokens: [],
      );

      _testIsolated('arrow',
        token: '->',
        type: TokenType.arrow,
      );

      _testIsolated('whitespace',
        token: ' ',
        type: TokenType.whitespace,
      );

      _testIsolated('number',
        token: '123456',
        type: TokenType.number,
      );

      _testIsolated('word addDrawing',
        token: 'addDrawing',
        type: TokenType.word,
      );

      _testIsolated('word next',
        token: 'next',
        type: TokenType.word,
      );

      _testIsolated('htmlrgb',
        token: '#ffaa88',
        type: TokenType.htmlrgb,
      );

      _testIsolated('punctuation',
        token: ',',
        type: TokenType.punctuation,
      );

      _testIsolated('percentage',
        token: '80%',
        type: TokenType.percentage,
      );

      _testIsolated('string',
        token: '"devkit"',
        type: TokenType.string,
      );

      _testIsolated('byte',
        token: '0xa7',
        type: TokenType.byte,
      );
    });

    group('medium', () {
      _test('function with params',
        tangleCode: 'animNone(1000);',
        tokens: [
          TangleToken(token: 'animNone', type: TokenType.word,  ),
          TangleToken(token: '(', type: TokenType.punctuation,  ),
          TangleToken(token: '1000', type: TokenType.number,  ),
          TangleToken(token: ')', type: TokenType.punctuation,  ),
          TangleToken(token: ';', type: TokenType.punctuation,  ),
        ],
      );
    });

    group('end-to-end', () {
      _test('"Green blink" animation',
          tangleCode: 'addDrawing(0, 2147483647, animFill(1000, #00ff00).next(animNone(1000)));',
          tokens: [
            TangleToken(token: 'addDrawing', type: TokenType.word,  ),
            TangleToken(token: '(', type: TokenType.punctuation,  ),
            TangleToken(token: '0', type: TokenType.number,  ),
            TangleToken(token: ',', type: TokenType.punctuation,  ),
            TangleToken(token: ' ', type: TokenType.whitespace,  ),
            TangleToken(token: '2147483647', type: TokenType.number,  ),
            TangleToken(token: ',', type: TokenType.punctuation,  ),
            TangleToken(token: ' ', type: TokenType.whitespace,  ),
            TangleToken(token: 'animFill', type: TokenType.word,  ),
            TangleToken(token: '(', type: TokenType.punctuation,  ),
            TangleToken(token: '1000', type: TokenType.number,  ),
            TangleToken(token: ',', type: TokenType.punctuation,  ),
            TangleToken(token: ' ', type: TokenType.whitespace,  ),
            TangleToken(token: '#00ff00', type: TokenType.htmlrgb,  ),
            TangleToken(token: ')', type: TokenType.punctuation,  ),
            TangleToken(token: '.', type: TokenType.punctuation,  ),
            TangleToken(token: 'next', type: TokenType.word,  ),
            TangleToken(token: '(', type: TokenType.punctuation,  ),
            TangleToken(token: 'animNone', type: TokenType.word,  ),
            TangleToken(token: '(', type: TokenType.punctuation,  ),
            TangleToken(token: '1000', type: TokenType.number,  ),
            TangleToken(token: ')', type: TokenType.punctuation,  ),
            TangleToken(token: ')', type: TokenType.punctuation,  ),
            TangleToken(token: ')', type: TokenType.punctuation,  ),
            TangleToken(token: ';', type: TokenType.punctuation,  ),
          ],
      );

      _test('"Rainbow night rider" animation',
          tangleCode: 'addDrawing(0, 2147483647, animRainbow(30000, 80%));\n'
              'filDrawing(0, 2147483647, animPlasmaShot(1000, #ffffff, 50%).next(animPlasmaShot(-1000, #ffffff, 50%)));',
          tokens: [
            TangleToken(token: 'addDrawing', type: TokenType.word,  ),
            TangleToken(token: '(', type: TokenType.punctuation,  ),
            TangleToken(token: '0', type: TokenType.number,  ),
            TangleToken(token: ',', type: TokenType.punctuation,  ),
            TangleToken(token: ' ', type: TokenType.whitespace,  ),
            TangleToken(token: '2147483647', type: TokenType.number,  ),
            TangleToken(token: ',', type: TokenType.punctuation,  ),
            TangleToken(token: ' ', type: TokenType.whitespace,  ),
            TangleToken(token: 'animRainbow', type: TokenType.word,  ),
            TangleToken(token: '(', type: TokenType.punctuation,  ),
            TangleToken(token: '30000', type: TokenType.number,  ),
            TangleToken(token: ',', type: TokenType.punctuation,  ),
            TangleToken(token: ' ', type: TokenType.whitespace,  ),
            TangleToken(token: '80%', type: TokenType.percentage, ),
            TangleToken(token: ')', type: TokenType.punctuation,  ),
            TangleToken(token: ')', type: TokenType.punctuation,  ),
            TangleToken(token: ';', type: TokenType.punctuation,  ),
            TangleToken(token: '\n', type: TokenType.whitespace,  ),

            TangleToken(token: 'filDrawing', type: TokenType.word,  ),
            TangleToken(token: '(', type: TokenType.punctuation,  ),
            TangleToken(token: '0', type: TokenType.number,  ),
            TangleToken(token: ',', type: TokenType.punctuation,  ),
            TangleToken(token: ' ', type: TokenType.whitespace,  ),
            TangleToken(token: '2147483647', type: TokenType.number,  ),
            TangleToken(token: ',', type: TokenType.punctuation,  ),
            TangleToken(token: ' ', type: TokenType.whitespace,  ),
            TangleToken(token: 'animPlasmaShot', type: TokenType.word,  ),
            TangleToken(token: '(', type: TokenType.punctuation,  ),
            TangleToken(token: '1000', type: TokenType.number,  ),
            TangleToken(token: ',', type: TokenType.punctuation,  ),
            TangleToken(token: ' ', type: TokenType.whitespace,  ),
            TangleToken(token: '#ffffff', type: TokenType.htmlrgb,  ),
            TangleToken(token: ',', type: TokenType.punctuation,  ),
            TangleToken(token: ' ', type: TokenType.whitespace,  ),
            TangleToken(token: '50%', type: TokenType.percentage, ),
            TangleToken(token: ')', type: TokenType.punctuation,  ),
            TangleToken(token: '.', type: TokenType.punctuation,  ),
            TangleToken(token: 'next', type: TokenType.word,  ),
            TangleToken(token: '(', type: TokenType.punctuation,  ),
            TangleToken(token: 'animPlasmaShot', type: TokenType.word,  ),
            TangleToken(token: '(', type: TokenType.punctuation,  ),
            TangleToken(token: '-1000', type: TokenType.number,  ),
            TangleToken(token: ',', type: TokenType.punctuation,  ),
            TangleToken(token: ' ', type: TokenType.whitespace,  ),
            TangleToken(token: '#ffffff', type: TokenType.htmlrgb,  ),
            TangleToken(token: ',', type: TokenType.punctuation,  ),
            TangleToken(token: ' ', type: TokenType.whitespace,  ),
            TangleToken(token: '50%', type: TokenType.percentage, ),
            TangleToken(token: ')', type: TokenType.punctuation,  ),
            TangleToken(token: ')', type: TokenType.punctuation,  ),
            TangleToken(token: ')', type: TokenType.punctuation,  ),
            TangleToken(token: ';', type: TokenType.punctuation,  ),
          ],
      );
    });
  });
}
