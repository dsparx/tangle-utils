import 'dart:typed_data';

import 'package:flutter_test/flutter_test.dart';
import 'package:spectoda_utils/src/tangle_parser/tangle_code_parser.dart';

void main() {
  final TangleCodeParser parser = TangleCodeParser();

  void _testToBytes(String description, {
    required String tangleCode,
    required List<int> output,
  }) {
    test(description, () {
      final Uint8List bytes = parser.toBytes(tangleCode);
      expect(bytes, output);
    });
  }

  group('TangleParser', () {
    group('toBigEndianBytes', () {
      test('255', () {
        expect(parser.toBigEndianBytes(255), [255, 0, 0, 0]);
      });

      test('256', () {
        expect(parser.toBigEndianBytes(256), [0, 1, 0, 0]);
      });
    });

    group('toBytes', () {
      group('simple', () {
        _testToBytes('empty',
            tangleCode: '',
            output: [248, 1, 0, 0, 0, 255],
        );

        _testToBytes('whitespace',
            tangleCode: ' ',
            output: [248, 1, 0, 0, 0, 255],
        );

        _testToBytes('comment',
            tangleCode: '// Comment example',
            output: [248, 1, 0, 0, 0, 255],
        );

        group('percentage', () {
          _testToBytes('100%',
              tangleCode: '100%',
              output: [248, 6, 0, 0, 0, 190, 255, 255, 255, 127, 255],
          );

          _testToBytes('50%',
              tangleCode: '50%',
              output: [248, 6, 0, 0, 0, 190, 255, 255, 255, 63, 255],
          );

          _testToBytes('10%',
              tangleCode: '10%',
              output: [248, 6, 0, 0, 0, 190, 204, 204, 204, 12, 255],
          );

          _testToBytes('0%',
              tangleCode: '0%',
              output: [248, 6, 0, 0, 0, 190, 0, 0, 0, 0, 255],
          );
        });

        group('infinity', () {
          _testToBytes('simple',
              tangleCode: 'Infinity',
              output: [248, 2, 0, 0, 0, 195, 255],
          );

          _testToBytes('positive',
              tangleCode: '+Infinity',
              output: [248, 2, 0, 0, 0, 195, 255],
          );

          _testToBytes('negative',
              tangleCode: '-Infinity',
              output: [248, 2, 0, 0, 0, 196, 255],
          );
        });

        group('word', () {
          _testToBytes('addDrawing',
              tangleCode: 'addDrawing',
              output: [248, 2, 0, 0, 0, 2, 255],
          );

          _testToBytes('animFill',
              tangleCode: 'animFill',
              output: [248, 2, 0, 0, 0, 33, 255],
          );

          _testToBytes('true',
              tangleCode: 'true',
              output: [248, 2, 0, 0, 0, 1, 255],
          );

          _testToBytes('false',
              tangleCode: 'false',
              output: [248, 2, 0, 0, 0, 0, 255],
          );
        });

        _testToBytes('end of statement',
            tangleCode: '}',
            output: [248, 2, 0, 0, 0, 254, 255],
        );

        group('timestamp', () {
          _testToBytes('timestamp',
              tangleCode: '5s',
              output: [248, 6, 0, 0, 0, 188, 136, 19, 0, 0, 255],
          );

          _testToBytes('timestamp',
              tangleCode: '0s',
              output: [248, 2, 0, 0, 0, 194, 255],
          );
        });

        group('color', () {
          _testToBytes('black',
              tangleCode: '#000000',
              output: [248, 2, 0, 0, 0, 198, 255],
          );

          _testToBytes('white',
              tangleCode: '#ffffff',
              output: [248, 2, 0, 0, 0, 197, 255],
          );

          _testToBytes('htmlrgb',
              tangleCode: '#11ffab',
              output: [248, 5, 0, 0, 0, 189, 17, 255, 171, 255],
          );
        });

        _testToBytes('char',
            tangleCode: "'B'",
            output: [248, 2, 0, 0, 0, 66, 255],
        );

        group('label', () {
          _testToBytes('short',
              tangleCode: r'$dev1',
              output: [248, 7, 0, 0, 0, 191, 100, 101, 118, 49, 0, 255],
          );

          _testToBytes('long',
              tangleCode: r'$dev1234',
              output: [248, 7, 0, 0, 0, 191, 100, 101, 118, 49, 50, 255],
          );
        });


        _testToBytes('string shorter then 8',
            tangleCode: '"devkit"',
            output: [248, 9, 0, 0, 0, 100, 101, 118, 107, 105, 116, 0, 0, 255],
        );

        _testToBytes('string exactly 8',
            tangleCode: '"devkitAB"',
            output: [248, 9, 0, 0, 0, 100, 101, 118, 107, 105, 116, 65, 66, 255],
        );

        _testToBytes('string longer then 8',
            tangleCode: '"devkitDCBE"',
            output: [248, 9, 0, 0, 0, 100, 101, 118, 107, 105, 116, 68, 67, 255],
        );

        _testToBytes('byte',
            tangleCode: '0xa7',
            output: [248, 2, 0, 0, 0, 167, 255],
        );
      });

      group('medium', () {
        _testToBytes('solid color',
            tangleCode: 'addDrawing(0s, 5s, animFill(5s, #ff0000));',
            output: [
              248, 18, 0, 0, 0,
              2, 194, 188, 136, 19, 0, 0, 33, 188, 136, 19, 0, 0, 189, 255, 0, 0, 255
            ],
        );

        _testToBytes('defDevice',
            tangleCode: r'defDevice($dev1, 0x05, 0xbf, 0x0f, 1px, 2px, 3px, 4px)',
            output: [
              248, 23, 0, 0, 0,
              24, 191, 100, 101, 118, 49, 0, 5, 191, 15, 192, 1, 0, 192, 2,
              0, 192, 3, 0, 192, 4, 0, 255
            ],
        );

        _testToBytes('defTangle',
            tangleCode: r"defTangle($tan1, { port($dev1, 'A'); });",
            output: [
              248, 17, 0, 0, 0,
              25, 191, 116, 97, 110, 49, 0, 179, 191, 100, 101, 118, 49, 0,
              65, 254, 255
            ],
        );

        _testToBytes('event',
            tangleCode: r'''
              interactive(0s, Infinity, $evt1, {
                addDrawing(0s, 1s, animFill(5s, #0000ab));
              });
            ''',
            output: [
              248, 28, 0, 0, 0,
              16, 194, 195, 191, 101, 118, 116, 49, 0, 2, 194, 188, 232, 3,
              0, 0, 33, 188, 136, 19, 0, 0, 189, 0, 0, 171, 254, 255
            ],
        );
      });

      group('end-to-end', () {
        _testToBytes('Plasma shot animation',
            tangleCode: r'''
              defDevice($dev1, 0x00, 0xff, 0x0f, 8px);
              addDrawing(0s, Infinity, animPlasmaShot(1s, #ff00ff, 100%));
            ''',
            output: [
              248, 32, 0, 0, 0,
              24, 191, 100, 101, 118, 49, 0, 0, 255, 15, 192, 8, 0, 2, 194,
              195, 36, 188, 232, 3, 0, 0, 189, 255, 0, 255, 190, 255, 255, 255,
              127, 255
            ],
        );
      });
    });
  });
}
