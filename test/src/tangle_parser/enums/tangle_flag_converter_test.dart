import 'package:flutter_test/flutter_test.dart';
import 'package:spectoda_utils/src/tangle_parser/enums/tangle_flag.dart';

void main() {
  final TangleFlagConverter converter = TangleFlagConverter();

  group('FlagsConverter', () {
    group('toByte', () {
      test('endOfTangleBytes', () {
        expect(
            converter.toByte(TangleFlag.defineVariable),
            18,
        );
      });

      test('generatorSine', () {
        expect(
            converter.toByte(TangleFlag.generatorSine),
            146,
        );
      });

      test('endOfTangleBytes', () {
        expect(
            converter.toByte(TangleFlag.endOfTangleBytes),
            255,
        );
      });
    });

    group('fromWordToByte', () {
      test('valid', () {
        expect(
            converter.fromWordToByte('emitAs'),
            185,
        );
      });

      test('invalid', () {
        expect(
            () => converter.fromWordToByte('abc'),
            throwsException,
        );
      });
    });
  });
}
