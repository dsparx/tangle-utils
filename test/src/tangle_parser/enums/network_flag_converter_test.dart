import 'package:flutter_test/flutter_test.dart';
import 'package:spectoda_utils/src/tangle_parser/enums/network_flag.dart';

void main() {
  final converter = NetworkFlagConverter();

  group('NetworkFlagConverter', () {
    group('toByte', () {
      test('emitColorEvent', () {
        expect(
            converter.toByte(NetworkFlag.flagEmitColorEvent),
            251,
        );
      });
    });

    group('fromByte', () {
      test('valid', () {
        expect(
            converter.fromByte(251),
            NetworkFlag.flagEmitColorEvent,
        );
      });

      test('invalid', () {
        expect(
            () => converter.fromByte(999),
            throwsException,
        );
      });
    });
  });
}
