import 'package:flutter_test/flutter_test.dart';
import 'package:spectoda_utils/src/tangle_parser/enums/token_type.dart';

void main() {
  final TokenTypeMatcher matcher = TokenTypeMatcher();

  void _test(String description, {
    required TokenType tokenType,
    required String input,
    required List<String> output,
  }) {
    test(description, () {
      expect(
        matcher.allMatches(type: tokenType, input: input),
        output,
      );
    });
  }

  group('TokenTypeMatcher', () {
    _test('comment',
        tokenType: TokenType.comment,
        input: 'abc //a comment',
        output: ['//a comment'],
    );

    _test('htmlrgb',
        tokenType: TokenType.htmlrgb,
        input: 'a #11ffaa b',
        output: ['#11ffaa'],
    );

    group('inifinity', () {
      _test('simple',
          tokenType: TokenType.infinity,
          input: 'a Infinity b',
          output: ['Infinity'],
      );

      _test('positive',
          tokenType: TokenType.infinity,
          input: 'a +Infinity b',
          output: ['+Infinity'],
      );

      _test('negative',
          tokenType: TokenType.infinity,
          input: 'a -Infinity b',
          output: ['-Infinity'],
      );
    });

    _test('timestamp',
      tokenType: TokenType.timestamp,
      input: 'a 16d15h14s b',
      output: ['16d15h14s'],
    );

    _test('arrow',
        tokenType: TokenType.arrow,
        input: 'a -> b',
        output: ['->'],
    );

    _test('whitespace',
        tokenType: TokenType.whitespace,
        input: 'a b  c',
        output: [' ', '  '],
    );

    _test('punctuation',
        tokenType: TokenType.punctuation,
        input: 'a . c , 56 ()',
        output: ['.', ',', '(', ')'],
    );
  });
}
