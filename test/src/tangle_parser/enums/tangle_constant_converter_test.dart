import 'package:flutter_test/flutter_test.dart';
import 'package:spectoda_utils/src/tangle_parser/enums/tangle_constant.dart';

void main() {
  final TangleConstantConverter converter = TangleConstantConverter();

  group('ConstantConverter', () {
    group('toByte', () {
      test('generatorSine', () {
        expect(
            converter.toByte(TangleConstant.modifierSwitchGB),
            2,
        );
      });

      test('true', () {
        expect(
            converter.toByte(TangleConstant.constantTrue),
            0x01,
        );
      });
    });

    group('fromWordToByte', () {
      test('valid modifier', () {
        expect(
            converter.fromWordToByte('MODIFIER_SWITCH_RB'),
            3,
        );
      });

      test('valid false', () {
        expect(
            converter.fromWordToByte('false'),
            0x00,
        );
      });

      test('invalid', () {
        expect(
            () => converter.fromWordToByte('abc'),
            throwsException,
        );
      });
    });
  });
}
