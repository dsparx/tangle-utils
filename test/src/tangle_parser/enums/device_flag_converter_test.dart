import 'package:flutter_test/flutter_test.dart';
import 'package:spectoda_utils/src/tangle_parser/enums/device_flag.dart';

void main() {
  final converter = DeviceFlagConverter();

  group('DeviceFlagConverter', () {
    group('toByte', () {
      test('emitColorEvent', () {
        expect(
            converter.toByte(DeviceFlag.flagAdoptRequest),
            240,
        );
      });
    });
  });
}
