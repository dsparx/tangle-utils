import 'dart:typed_data';

import 'package:flutter_test/flutter_test.dart';
import 'package:spectoda_utils/src/tangle_parser/tangle_reader.dart';

void main() {
  final reader = TangleReader();

  group('TangleReader', () {
    group('isolated', () {
      test('Uint8', () {
        reader.setDataToRead(Uint8List.fromList([10]));
        expect(reader.readUint8(), 10);
      });

      test('Flag', () {
        reader.setDataToRead(Uint8List.fromList([10]));
        expect(reader.readFlag(), 10);
      });

      test('Uint32', () {
        reader.setDataToRead(Uint8List.fromList([255, 255, 255, 255]));
        expect(reader.readUint32(), 4294967295);
      });

      test('Int32', () {
        reader.setDataToRead(Uint8List.fromList([255, 255, 255, 255]));
        expect(reader.readInt32(), 4294967295);
      });

      test('Int32', () {
        reader.setDataToRead(Uint8List.fromList([10, 20, 30]));
        expect(reader.readBytes(2), [10, 20]);
      });

      test('Percentage', () {
        reader.setDataToRead(Uint8List.fromList([16, 8, 35, 77]));
        expect(reader.readPercentage(), 60);
      });

      test('Bytes', () {
        reader.setDataToRead(Uint8List.fromList([1, 2, 3]));
        expect(reader.readBytes(3), [1, 2, 3]);
      });
    });

    group('multiple', () {
      test('2x Uint8', () {
        reader.setDataToRead(Uint8List.fromList([10, 20]));
        expect(reader.readUint8(), 10);
        expect(reader.readUint8(), 20);
      });

      test('Flag followed by Uint', () {
        reader.setDataToRead(Uint8List.fromList([10, 0, 0, 0, 128]));
        expect(reader.readFlag(), 10);
        expect(reader.readUint32(), 2147483648);
      });

      test('Bytes followed by flag', () {
        reader.setDataToRead(Uint8List.fromList([10, 0, 0, 0, 99]));
        expect(reader.readBytes(4), [10, 0, 0, 0]);
        expect(reader.readFlag(), 99);
      });
    });

    group('out of range', () {
      test('Bytes', () {
        reader.setDataToRead(Uint8List.fromList([1, 2, 3]));
        expect(() => reader.readBytes(4), throwsException);
      });

      test('readValue', () {
        reader.setDataToRead(Uint8List.fromList([]));
        expect(() => reader.readFlag(), throwsException);
      });

      test('peekValue', () {
        reader.setDataToRead(Uint8List.fromList([]));
        expect(() => reader.peekValue(1, isUnsigned: false), throwsException);
      });
    });
  });
}
