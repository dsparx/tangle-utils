import 'package:flutter_test/flutter_test.dart';
import 'package:spectoda_utils/src/tangle_parser/enums/network_flag.dart';
import 'package:spectoda_utils/src/tangle_parser/tangle_decoder.dart';

void main() {
  final decoder = TangleDecoder();

  group('TangleDecoder', () {
    group('device', () {
      group('adoptResponse', () {
        test('valid', () {
          final deviceMac = decoder.adoptResponse(requestUUID: 1, response: [
            241,
            1, 0, 0, 0,
            0,
            1, 2, 3, 4, 5, 6
          ],);
          expect(deviceMac, [1, 2, 3, 4, 5, 6]);
        });

        test('invalid response flag', () {
          expect(() => decoder.adoptResponse(requestUUID: 1, response: [
            999,
            1, 0, 0, 0,
            0,
            1, 2, 3, 4, 5, 6
          ],), throwsException,);
        });

        test('invalid response uuid', () {
          expect(() => decoder.adoptResponse(requestUUID: 1, response: [
            241,
            999, 0, 0, 0,
            0,
            1, 2, 3, 4, 5, 6
          ],), throwsException,);
        });

        test('error code not zero', () {
          expect(() => decoder.adoptResponse(requestUUID: 1, response: [
            241,
            1, 0, 0, 0,
            999,
            1, 2, 3, 4, 5, 6
          ],), throwsException,);
        });
      });
    });

    group('network', () {
      test('networkEventType analyzes first byte of response', () {
        expect(
          decoder.networkEventType(response: [252, 0, 0, 0]),
          NetworkFlag.flagEmitPercentageEvent,
        );
      });

      group('batteryEvent', () {
        test('valid', () {
          final battery = decoder.batteryEvent(response: [
            252,
            16, 8, 35, 77 ,
            1, 2, 3, 4, 5,
            1, 2, 3, 4,
            2,
          ],);
          expect(battery.percentage, 60);
          expect(battery.deviceID, 2);
        });

        test('invalid flag', () {
          expect(() => decoder.batteryEvent(response: [
            999,
            16, 8, 35, 77 ,
            1, 2, 3, 4, 5,
            1, 2, 3, 4,
            0,
          ],), throwsException,);
        });
      });
    });
  });
}
