import 'package:flutter_test/flutter_test.dart';
import 'package:spectoda_utils/src/tangle_builder/tangle_builder.dart';
import 'package:spectoda_utils/src/tangle_parser/enums/tangle_flag.dart';

void main() {
  final TangleBuilder builder = TangleBuilder();

  void expectCode(String matcher) {
    expect(builder.toCode(), equalsIgnoringWhitespace(matcher));
  }

  group('buffer manipulation', () {
    test('write', () {
      builder.write('x');
      expectCode('x');
    });

    test('semicolon', () {
      builder.semicolon();
      expectCode(';\n');
    });

    test('writeN', () {
      builder.writeN('x');
      expectCode('x\n');
    });

    test('writeMoreN', () {
      builder.writeMoreN(['x', 'y']);
      expectCode('x\ny\n');
    });

    test('writeInBrackets', () {
      builder.writeInBrackets(() => builder.write('x'));
      expectCode('{\nx\n}');
    });
  });

  group('utils', () {
    group('appendBody', () {
      test('null', () {
        builder.appendBody(null);
        expectCode(')');
      });

      test('writeBodyParams', () {
        builder.appendBody(() => builder.write('x'));
        expectCode(', {\nx\n})');
      });
    });

    group('parentheses', () {
      test('params', () {
        builder.parentheses(TangleFlag.defineDevice, params: 'x');
        expectCode('defDevice(x)');
      });

      test('params with body', () {
        builder.parentheses(TangleFlag.defineDevice, params: 'x', body: () => builder.write('body'));
        expectCode('defDevice(x, { body })');
      });

      test('bodyParams with body', () {
        builder.parentheses(TangleFlag.defineDevice, bodyParams: () => builder.write('x'), body: () => builder.write('body'));
        expectCode('defDevice({ x }, { body })');
      });
    });

    group('function', () {
      test('bodyParams with body', () {
        builder.function(TangleFlag.defineDevice, bodyParams: () => builder.write('x'), body: () => builder.write('body'));
        expectCode('defDevice({ x }, { body });');
      });
    });
  });

  group('atomic', () {

  });

  group('defineDevices', () {
    test('3 devices, default pixels', () {
      builder.defineDevices([0, 1, 2]);
      expectCode(r'''
        defDevice($dev0, 0x00, 0xff, 0x0f, 28px);
        defDevice($dev1, 0x01, 0xff, 0x0f, 28px);
        defDevice($dev2, 0x02, 0xff, 0x0f, 28px);
      ''',);
    });

    test('addDrawing', () {
      builder.addDrawing(3, 2, 'dummy code');
      expectCode('addDrawing(3.0s, 2.0s, dummy code);');
    });
  });

  group('addWindow', () {
    test('without length nor timeloop', () {
      builder.addWindow(modifiers: [], body: () => builder.writeN('dummy code'));
      expectCode('''
        addWindow(0s, Infinity, {
          dummy code
        });
      ''',);
    });

    test('with length and timeloop', () {
      builder.addWindow(
        windowDuration: 5,
        modifiers: [() => builder.modifyTimeLoop(10)],
        body: () => builder.writeN('dummy code'),
      );
      expectCode('''
        addWindow(0s, 5.0s, {
          dummy code
        }).modifyTimeLoop(10.0s);
      ''',);
    });
  });

  group('apply on tangles', () {
    test('1, 2, 3', () {
      builder.siftTangles([1, 2, 3], () => builder.writeN('dummy code'));
      expectCode(r'''
        siftTangles({
          tangle($tan1);
          tangle($tan2);
          tangle($tan3);
        }, {
          dummy code
        });
      ''',);
    });
  });

}
