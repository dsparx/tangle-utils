import 'package:spectoda_utils/src/tangle_parser/enums/tangle_flag.dart';

typedef Callback = void Function();

class TangleBuilder {
  final converter = TangleFlagConverter();

  final buffer = StringBuffer();

  String toCode() {
    final String code = buffer.toString();
    buffer.clear();
    return code;
  }

  // Wrappers

  void siftTangles(Iterable<int> tangleNums, Callback body) {
    return function(TangleFlag.sifterTangle,
        bodyParams: () => tangles(tangleNums),
        body: body
    );
  }

  void defineDevices(Iterable<int> deviceNums, {int pixels = 28}) {
    for (final num in deviceNums) {
      defineDevice(num);
    }
  }

  void tangles(Iterable<int> tangleNums) {
    for (final num in tangleNums) {
      tangle(num);
    }
  }

  void addWindow({double? windowDuration, required Callback body, Iterable<Callback>? modifiers}) {
    final String duration = windowDuration == null ? 'Infinity' : '${windowDuration}s';
    parentheses(TangleFlag.windowAdd, params: '0s, $duration', body: body);

    if (modifiers == null || modifiers.isEmpty) {
      semicolon();
    } else {
      chain(modifiers, separator: '.', separatorPrefix: true);
    }
  }

  // Atomic

  void defineDevice(int number, {int pixels = 28}) {
    function(TangleFlag.defineDevice, params: '\$dev$number, 0x0$number, 0xff, 0x0f, ${pixels}px');
  }

  void defineTangle(int tangleNum, {required Callback body}) {
    function(TangleFlag.defineTangle, params: '\$tan$tangleNum', body: body);
  }

  void definePort(int deviceNum, {String port = 'A'}) {
    function(TangleFlag.port, params: "\$dev$deviceNum, '$port'");
  }

  void defineSlice(int tangleNum, {required int from, required int length, int step = 1}) {
    function(TangleFlag.slice, params: '\$tan$tangleNum, ${from}px, ${length}px, ${step}px');
  }

  void defineVariable(String name) {
    assert(name.length <= 6);
    function(TangleFlag.defineVariable, params: '\$$name, genLastEventParam(\$$name)');
  }

  void modifyTimeLoop(double loopDuration) {
    parentheses(TangleFlag.modifierTimeLoop, params: '${loopDuration}s');
  }

  void modifyBrightness(String variableName) {
    parentheses(TangleFlag.modifierBrightness, params: '\$$variableName');
  }

  void tangle(int num) {
    function(TangleFlag.tangle, params: '\$tan$num');
  }

  void addDrawing(double from, double length, String animation) {
    function(TangleFlag.drawingAdd, params: '${from}s, ${length}s, $animation');
  }

  // Utils

  //void params(Iterable<String> params) {
  //  chain(params.map((e) => () => write(e)), separator: ', ');
  //}

  void chain(Iterable<Callback> functions, {required String separator, bool separatorPrefix = false}) {
    bool writeSeparator = separatorPrefix;
    for (final function in functions) {
      if (writeSeparator) {
        write(separator);
      } else {
        writeSeparator = true;
      }
      function();
    }
    semicolon();
  }

  void function(TangleFlag flag, {String? params, Callback? bodyParams, Callback? body}) {
    parentheses(flag, params: params, bodyParams: bodyParams, body: body);
    semicolon();
  }

  void parentheses(TangleFlag flag, {String? params, Callback? bodyParams, Callback? body}) {
    assert(params != null || bodyParams != null);
    assert(params == null || bodyParams == null);

    final key = converter.toWord(flag);
    write('$key(');

    if (params != null) {
      write(params);
    } else if (bodyParams != null) {
      writeInBrackets(bodyParams);
    }
    appendBody(body);
  }

  void appendBody(Callback? body) {
    if (body != null) {
      write(', ');
      writeInBrackets(body);
    }
    write(')');
  }

  // Atomic utils

  void writeInBrackets(Callback bodyParams) {
    writeN('{');
    bodyParams();
    writeN('');
    write('}');
  }

  void writeMoreN(Iterable<String> strList) {
    strList.forEach(writeN);
  }

  void semicolon() => writeN(';');

  void writeN(String str) {
    write(str + '\n');
  }

  void write(String str) => buffer.write(str);
}
