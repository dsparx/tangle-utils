import 'package:spectoda_utils/src/models/battery_event.dart';

class Battery {
  final int percentage;
  final int deviceID;
  final ChargingEvent chargingEvent;
  final Duration timestamp;

  Battery({
    required this.percentage,
    required this.chargingEvent,
    required this.deviceID,
    required this.timestamp,
  });
}
