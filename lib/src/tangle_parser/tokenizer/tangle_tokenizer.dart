import 'package:spectoda_utils/src/tangle_parser/enums/token_type.dart';
import 'package:spectoda_utils/src/tangle_parser/tokenizer/tangle_token.dart';
class TangleTokenizer {
  final TokenTypeMatcher matcher = TokenTypeMatcher();

  List<TangleToken> tokens = [];
  String remainingCode = '';
  int earliestIndex = 0;

  Iterable<TangleToken> tokenize(String tangleCode) {
    _setup(tangleCode);
    while (remainingCode.isNotEmpty) {
      _extractToken();
    }
    return tokens;
  }

  void _setup(String tangleCode) {
    tokens = [];
    remainingCode = tangleCode;
  }

  void _extractToken() {
    final TangleToken token = _regexp();
    tokens.add(token);
    _truncateCode(token);
  }

  void _truncateCode(TangleToken token) {
    final int start = earliestIndex + token.token.length;
    remainingCode = remainingCode.substring(start);
    earliestIndex = remainingCode.length;
  }

  TangleToken _regexp() {
    TangleToken? token;
    earliestIndex = remainingCode.length;

    for (final TokenType type in TokenType.values) {
      final Iterable<RegExpMatch> allMatches = matcher.allRegExpMatches(type: type, input: remainingCode);
      if (allMatches.isEmpty) continue;

      final RegExpMatch firstMatch = allMatches.first;
      if (firstMatch.start < earliestIndex) {
        token = TangleToken(
          token: firstMatch.group(0) ?? '',
          type: type,
        );
        earliestIndex = firstMatch.start;
      }
    }
    if (earliestIndex != 0) {
      tokens.add(TangleToken(
        token: remainingCode.substring(0, earliestIndex),
        type: TokenType.comment, // INVALID TOKEN
      ),);
    }
    if (token == null) throw 'Token is null';
    return token;
  }
}
