import 'package:spectoda_utils/src/tangle_parser/enums/token_type.dart';

class TangleToken {
  String token;
  TokenType type;

  TangleToken({
    required this.token,
    required this.type,
  });
}
