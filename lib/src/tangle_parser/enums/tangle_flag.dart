class TangleFlagConverter {
  int fromWordToByte(String word) {
    return toByte(_fromTangleWord(word));
  }

  String toWord(TangleFlag flag) {
    final inverse = _wordsAsFlag.map((k, v) => MapEntry(v, k));
    final String? word = inverse[flag];
    if (word == null) {
      throw Exception('Flag "$flag" could not be transferred to word');
    }
    return word;
  }

  int toByte(TangleFlag flag) {
    final int? byte = _flagsAsBytes[flag];
    if (byte == null) {
      throw Exception('Flag "$flag" could not be transferred to byte');
    }
    return byte;
  }

  TangleFlag _fromTangleWord(String word) {
    final TangleFlag? flag = _wordsAsFlag[word];
    if (flag == null) {
      throw Exception('Word "$word" could not be transferred to flag');
    }
    return flag;
  }
}

final Map<String, TangleFlag> _wordsAsFlag = {
  /* no code or command used by decoder as a validation */
  //'': TangleFlag.none,

  /* drawings */
  'setDrawing': TangleFlag.drawingSet,
  'addDrawing': TangleFlag.drawingAdd,
  'subDrawing': TangleFlag.drawingSub,
  'scaDrawing': TangleFlag.drawingScale,
  'filDrawing': TangleFlag.drawingFilter,

  /* windows */
  'setWindow': TangleFlag.windowSet,
  'addWindow': TangleFlag.windowAdd,
  'subWindow': TangleFlag.windowSub,
  'scaWindow': TangleFlag.windowScale,
  'filWindow': TangleFlag.windowFilter,

  /* frame */
  'frame': TangleFlag.frame,

  /* clip */
  'clip': TangleFlag.clip,

  /* sifters */
  'siftDevices': TangleFlag.sifterDevice,
  'siftTangles': TangleFlag.sifterTangle,
  'siftGroups': TangleFlag.sifterGroup,

  /* event handlers */
  'interactive': TangleFlag.interactive,
  'handleEvent': TangleFlag.eventHandle,

  /* definitions scoped */
  'defVariable': TangleFlag.defineVariable,

  /* definitions global */
  'defDevice': TangleFlag.defineDevice,
  'defTangle': TangleFlag.defineTangle,
  'defGroup': TangleFlag.defineGroup,
  'defMarks': TangleFlag.defineMarks,
  'defAnimation': TangleFlag.defineAnimation,
  //'': TangleFlag.defineEmitter,

  /* animations */
  'animNone': TangleFlag.animationNone,
  'animFill': TangleFlag.animationFill,
  'animRainbow': TangleFlag.animationRainbow,
  'animFade': TangleFlag.animationFade,
  'animPlasmaShot': TangleFlag.animationProjectile,
  'animLoadingBar': TangleFlag.animationLoading,
  'animColorRoll': TangleFlag.animationColorRoll,
  'animPaletteRoll': TangleFlag.animationPaletteRoll,
  //'': TangleFlag.animationInlAni,
  'animDefined': TangleFlag.animationDefined,

  /* modifiers */
  'modifyBrightness': TangleFlag.modifierBrightness,
  'modifyTimeline': TangleFlag.modifierTimeline,
  'modifyFadeIn': TangleFlag.modifierFadeIn,
  'modifyFadeOut': TangleFlag.modifierFadeOut,
  'modifyColorSwitch': TangleFlag.modifierSwitchColors,
  'modifyTimeLoop': TangleFlag.modifierTimeLoop,
  'modifyTimeScale': TangleFlag.modifierTimeScale,
  'modifyTimeScaleSmoothed': TangleFlag.modifierTimeScaleSmoothed,
  'modifyTimeChange': TangleFlag.modifierTimeChange,
  'modifyTimeSet': TangleFlag.modifierTimeSet,

  /* events */
  'genLastEventParam': TangleFlag.generatorLastEventValue,
  'genSmoothOut': TangleFlag.generatorSmoothOut,
  'genSine': TangleFlag.generatorSine,
  'genSaw': TangleFlag.generatorSaw,
  'genTriangle': TangleFlag.generatorTriangle,
  'genSquare': TangleFlag.generatorSquare,
  'genPerlinNoise': TangleFlag.generatorPerlinNoise,

  /* variable operations gates */
  'variable': TangleFlag.variableRead,
  'addValues': TangleFlag.variableAdd,
  'subValues': TangleFlag.variableSub,
  'mulValues': TangleFlag.variableMul,
  'divValues': TangleFlag.variableDiv,
  'modValues': TangleFlag.variableMod,
  'scaValue': TangleFlag.variableScale,
  'mapValue': TangleFlag.variableMap,

  /* objects */
  'device': TangleFlag.device,
  'tangle': TangleFlag.tangle,
  'slice': TangleFlag.slice,
  'port': TangleFlag.port,
  'group': TangleFlag.group,
  'marks': TangleFlag.marks,

  /* events */
  'setValue': TangleFlag.evenSetValue,
  'emitAs': TangleFlag.evenEmitLocal,

  /* values */
  //'': TangleFlag.timestamp,
  //'': TangleFlag.color,
  //'': TangleFlag.percentage,
  //'': TangleFlag.label,
  //'': TangleFlag.pixels,
  //'': TangleFlag.tuple,

  /* most used constants */
  //'': TangleFlag.timestampZero,
  //'': TangleFlag.timestampMax,
  //'': TangleFlag.timestampMin,
  //'': TangleFlag.colorWhite,
  //'': TangleFlag.colorBlack,

  /* command flags */
  //'': TangleFlag.flagTangleBytes,
  //'': TangleFlag.flagSetTimeline,
  //'': TangleFlag.flagEmitTimestampEvent,
  //'': TangleFlag.flagEmitColorEvent,
  //'': TangleFlag.flagEmitPercentageEvent,
  //'': TangleFlag.flagEmitLabelEvent,

  /* command ends */
  //'': TangleFlag.endOfStatement,
  //'': TangleFlag.endOfTangleBytes,

};

final Map<TangleFlag, int> _flagsAsBytes = {
  /* no code or command used by decoder as a validation */
  TangleFlag.none: 0,

  /* drawings */
  TangleFlag.drawingSet: 1,
  TangleFlag.drawingAdd: 2,
  TangleFlag.drawingSub: 3,
  TangleFlag.drawingScale: 4,
  TangleFlag.drawingFilter: 5,

  /* windows */
  TangleFlag.windowSet: 6,
  TangleFlag.windowAdd: 7,
  TangleFlag.windowSub: 8,
  TangleFlag.windowScale: 9,
  TangleFlag.windowFilter: 10,

  /* frame */
  TangleFlag.frame: 11,

  /* clip */
  TangleFlag.clip: 12,

  /* sifters */
  TangleFlag.sifterDevice: 13,
  TangleFlag.sifterTangle: 14,
  TangleFlag.sifterGroup: 15,

  /* event handlers */
  TangleFlag.interactive: 16,
  TangleFlag.eventHandle: 17,

  /* definitions scoped */
  TangleFlag.defineVariable: 18,

  /* definitions global */
  TangleFlag.defineDevice: 24,
  TangleFlag.defineTangle: 25,
  TangleFlag.defineGroup: 26,
  TangleFlag.defineMarks: 27,
  TangleFlag.defineAnimation: 28,
  TangleFlag.defineEmitter: 28,

  /* animations */
  TangleFlag.animationNone: 32,
  TangleFlag.animationFill: 33,
  TangleFlag.animationRainbow: 34,
  TangleFlag.animationFade: 35,
  TangleFlag.animationProjectile: 36,
  TangleFlag.animationLoading: 37,
  TangleFlag.animationColorRoll: 38,
  TangleFlag.animationPaletteRoll: 39,
  TangleFlag.animationInlAni: 40,
  TangleFlag.animationDefined: 41,

  /* modifiers */
  TangleFlag.modifierBrightness: 128,
  TangleFlag.modifierTimeline: 129,
  TangleFlag.modifierFadeIn: 130,
  TangleFlag.modifierFadeOut: 131,
  TangleFlag.modifierSwitchColors: 132,
  TangleFlag.modifierTimeLoop: 133,
  TangleFlag.modifierTimeScale: 134,
  TangleFlag.modifierTimeScaleSmoothed: 135,
  TangleFlag.modifierTimeChange: 136,
  TangleFlag.modifierTimeSet: 137,

  /* events */
  TangleFlag.generatorLastEventValue: 144,
  TangleFlag.generatorSmoothOut: 145,
  TangleFlag.generatorSine: 146,
  TangleFlag.generatorSaw: 147,
  TangleFlag.generatorTriangle: 148,
  TangleFlag.generatorSquare: 149,
  TangleFlag.generatorPerlinNoise: 150,

  /* variable operations gates */
  TangleFlag.variableRead: 160,
  TangleFlag.variableAdd: 161,
  TangleFlag.variableSub: 162,
  TangleFlag.variableMul: 163,
  TangleFlag.variableDiv: 164,
  TangleFlag.variableMod: 165,
  TangleFlag.variableScale: 166,
  TangleFlag.variableMap: 167,

  /* objects */
  TangleFlag.device: 176,
  TangleFlag.tangle: 177,
  TangleFlag.slice: 178,
  TangleFlag.port: 179,
  TangleFlag.group: 180,
  TangleFlag.marks: 181,

  /* events */
  TangleFlag.evenSetValue: 184,
  TangleFlag.evenEmitLocal: 185,

  /* values */
  TangleFlag.timestamp: 188,
  TangleFlag.color: 189,
  TangleFlag.percentage: 190,
  TangleFlag.label: 191,
  TangleFlag.pixels: 192,
  TangleFlag.tuple: 193,

  /* most used constants */
  TangleFlag.timestampZero: 194,
  TangleFlag.timestampMax: 195,
  TangleFlag.timestampMin: 196,
  TangleFlag.colorWhite: 197,
  TangleFlag.colorBlack: 198,

  /* command ends */
  TangleFlag.endOfStatement: 254,
  TangleFlag.endOfTangleBytes: 255,
};

enum TangleFlag {
  /* no code or command used by decoder as a validation */
  none,

  /* drawings */
  drawingSet,
  drawingAdd,
  drawingSub,
  drawingScale,
  drawingFilter,

  /* windows */
  windowSet,
  windowAdd,
  windowSub,
  windowScale,
  windowFilter,

  /* frame */
  frame,

  /* clip */
  clip,

  /* sifters */
  sifterDevice,
  sifterTangle,
  sifterGroup,

  /* event handlers */
  interactive,
  eventHandle,

  /* definitions scoped */
  defineVariable,

  /* definitions global */
  defineDevice,
  defineTangle,
  defineGroup,
  defineMarks,
  defineAnimation,
  defineEmitter,

  /* animations */
  animationNone,
  animationFill,
  animationRainbow,
  animationFade,
  animationProjectile,
  animationLoading,
  animationColorRoll,
  animationPaletteRoll,
  animationInlAni,
  animationDefined,

  /* modifiers */
  modifierBrightness,
  modifierTimeline,
  modifierFadeIn,
  modifierFadeOut,
  modifierSwitchColors,
  modifierTimeLoop,
  modifierTimeScale,
  modifierTimeScaleSmoothed,
  modifierTimeChange,
  modifierTimeSet,

  /* events */
  generatorLastEventValue,
  generatorSmoothOut,
  generatorSine,
  generatorSaw,
  generatorTriangle,
  generatorSquare,
  generatorPerlinNoise,

  /* variable operations gates */
  variableRead,
  variableAdd,
  variableSub,
  variableMul,
  variableDiv,
  variableMod,
  variableScale,
  variableMap,

  /* objects */
  device,
  tangle,
  slice,
  port,
  group,
  marks,

  /* events */
  evenSetValue,
  evenEmitLocal,

  /* values */
  timestamp,
  color,
  percentage,
  label,
  pixels,
  tuple,

  /* most used constants */
  timestampZero,
  timestampMax,
  timestampMin,
  colorWhite,
  colorBlack,

  /* command ends */
  endOfStatement,
  endOfTangleBytes,
}
