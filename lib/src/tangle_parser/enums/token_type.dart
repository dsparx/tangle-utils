class TokenTypeMatcher {
  List<String> allMatches({required TokenType type, required String input}) {
    return allRegExpMatches(type: type, input: input)
        .map((match) => match.group(0) ?? 'error')
        .toList();
  }

  Iterable<RegExpMatch> allRegExpMatches({required TokenType type, required String input}) {
    final RegExp? regexp = _matchers[type];
    if (regexp == null) {
      throw Exception('No matching RegExp for TokenType "$type"');
    }
    return regexp.allMatches(input);
  }
}

final Map<TokenType, RegExp> _matchers = {
  TokenType.comment: RegExp(r'(//[^\n]*)'),
  TokenType.htmlrgb: RegExp('(#[0-9a-f]{6})', caseSensitive: false),
  TokenType.infinity: RegExp('([+-]?Infinity)'),
  TokenType.string: RegExp(r'("[\w ]*")'),
  TokenType.timestamp: RegExp('((_?[+-]?[0-9]*[.]?[0-9]+[dhmst])+)'),
  TokenType.label: RegExp(r'(\$[\w]*)'),
  TokenType.char: RegExp(r"(-?'[\W\w]')"),
  TokenType.byte: RegExp('(0x[0-9a-f][0-9a-f](?![0-9a-f]))', caseSensitive: false),
  TokenType.pixels: RegExp(r'([\d]+px)'),
  TokenType.percentage: RegExp(r'([+-]?[\d.]+%)'),
  TokenType.float: RegExp('([+-]?[0-9]*[.][0-9]+)'),
  TokenType.number: RegExp('([+-]?[0-9]+)'),
  TokenType.arrow: RegExp('(->)'),
  TokenType.word: RegExp(r'([a-z_][\w]*)', caseSensitive: false),
  TokenType.whitespace: RegExp(r'(\s+)'),
  TokenType.punctuation: RegExp(r'([^\w\s])'),
};

enum TokenType {
  comment,
  htmlrgb,
  infinity,
  string,
  timestamp,
  label,
  char,
  byte,
  pixels,
  percentage,
  float,
  number,
  arrow,
  word,
  whitespace,
  punctuation
}
