class NetworkFlagConverter {
  int toByte(NetworkFlag flag) {
    final int? byte = _flagsAsBytes[flag];
    if (byte == null) {
      throw Exception('Network flag "$flag" could not be transferred to byte');
    }
    return byte;
  }

  NetworkFlag fromByte(int byte) {
    for (final entry in _flagsAsBytes.entries) {
      if (byte == entry.value) {
        return entry.key;
      }
    }
    throw Exception('No NetworkFlag with value "$byte" found');
  }
}

final Map<NetworkFlag, int> _flagsAsBytes = {
  /* command flags */
  NetworkFlag.flagRSSIData: 100,
  NetworkFlag.flagPeerConnected: 101,
  NetworkFlag.flagPeerDisconnected: 102,

  NetworkFlag.deviceConfBytes: 240,
  NetworkFlag.flagTangleBytes: 248,
  NetworkFlag.flagSetTimeline: 249,

  NetworkFlag.flagEmitLazyEvent: 230,
  NetworkFlag.flagEmitLazyTimestampEvent: 231,
  NetworkFlag.flagEmitLazyColorEvent: 232,
  NetworkFlag.flagEmitLazyPercentageEvent: 233,
  NetworkFlag.flagEmitLazyLabelEvent: 234,

  NetworkFlag.flagEmitEvent: 247,
  NetworkFlag.flagEmitTimestampEvent: 250,
  NetworkFlag.flagEmitColorEvent: 251,
  NetworkFlag.flagEmitPercentageEvent: 252,
  NetworkFlag.flagEmitLabelEvent: 253,
};

enum NetworkFlag {
  /* command flags */
  flagRSSIData,
  flagPeerConnected,
  flagPeerDisconnected,

  deviceConfBytes,
  flagTangleBytes,
  flagSetTimeline,

  flagEmitLazyEvent,
  flagEmitLazyTimestampEvent,
  flagEmitLazyColorEvent,
  flagEmitLazyPercentageEvent,
  flagEmitLazyLabelEvent,

  flagEmitEvent,
  flagEmitTimestampEvent,
  flagEmitColorEvent,
  flagEmitPercentageEvent,
  flagEmitLabelEvent,
}
