class DeviceFlagConverter {
  int toByte(DeviceFlag flag) {
    final int? byte = _flagsAsBytes[flag];
    if (byte == null) {
      throw Exception('Device flag "$flag" could not be transferred to byte');
    }
    return byte;
  }
}

final Map<DeviceFlag, int> _flagsAsBytes = {
  // legacy
  DeviceFlag.flagOtaBegin: 255,
  DeviceFlag.flagOtaWrite: 0,
  DeviceFlag.flagOtaEnd: 254,
  DeviceFlag.flagOtaReset: 253,
  DeviceFlag.flagDeviceReboot: 5,

  DeviceFlag.flagConfigUpdateRequest: 10,
  DeviceFlag.flagConfigUpdateResponse: 11,

  DeviceFlag.flagSleepRequest: 222,
  DeviceFlag.flagSleepResponse: 223,
  DeviceFlag.flagConnectedPeersInfoRequest: 224,
  DeviceFlag.flagConnectedPeersInfoResponse: 225,

  DeviceFlag.flagRomPhyVDD33Request: 228,
  DeviceFlag.flagRomPhyVDD33Response: 229,
  DeviceFlag.flagVoltageOnPinRequest: 230,
  DeviceFlag.flagVoltageOnPinResponse: 231,

  DeviceFlag.flagChangeDataRateRequest: 232,
  DeviceFlag.flagChangeDataRateResponse: 233,

  DeviceFlag.flagFwVersionRequest: 234,
  DeviceFlag.flagFwVersionResponse: 235,
  DeviceFlag.flagEraseOwnerRequest: 236,
  DeviceFlag.flagEraseOwnerResponse: 237,

  DeviceFlag.flagTnglFingerprintRequest: 242,
  DeviceFlag.flagTnglFingerprintResponse: 243,
  DeviceFlag.flagTimelineRequest: 245,
  DeviceFlag.flagTimelineResponse: 246,

  DeviceFlag.flagConnectRequest: 238,
  DeviceFlag.flagConnectResponse: 239,
  DeviceFlag.flagAdoptRequest: 240,
  DeviceFlag.flagAdoptResponse: 241,
};

enum DeviceFlag {
  // legacy
  flagOtaBegin,
  flagOtaWrite,
  flagOtaEnd,
  flagOtaReset,
  flagDeviceReboot,

  flagConfigUpdateRequest,
  flagConfigUpdateResponse,

  flagSleepRequest,
  flagSleepResponse,
  flagConnectedPeersInfoRequest,
  flagConnectedPeersInfoResponse,

  flagRomPhyVDD33Request,
  flagRomPhyVDD33Response,
  flagVoltageOnPinRequest,
  flagVoltageOnPinResponse,

  flagChangeDataRateRequest,
  flagChangeDataRateResponse,

  flagFwVersionRequest,
  flagFwVersionResponse,
  flagEraseOwnerRequest,
  flagEraseOwnerResponse,

  flagTnglFingerprintRequest,
  flagTnglFingerprintResponse,

  flagTimelineRequest,
  flagTimelineResponse,

  flagConnectRequest,
  flagConnectResponse,

  flagAdoptRequest,
  flagAdoptResponse,
}
