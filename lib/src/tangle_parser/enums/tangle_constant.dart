class TangleConstantConverter {
  int fromWordToByte(String word) {
    return toByte(_fromTangleWord(word));
  }

  int toByte(TangleConstant constant) {
    final int? byte = _constantsAsBytes[constant];
    if (byte == null) {
      throw Exception('Constant "$constant" could not be transferred to byte');
    }
    return byte;
  }

  TangleConstant _fromTangleWord(String word) {
    final TangleConstant? constant = _wordsAsConstant[word];
    if (constant == null) {
      throw Exception('Word "$word" could not be transferred to constant');
    }
    return constant;
  }
}

final Map<String, TangleConstant> _wordsAsConstant = {
  'MODIFIER_SWITCH_NONE': TangleConstant.modifierSwitchNone,

  'MODIFIER_SWITCH_RG': TangleConstant.modifierSwitchRG,
  'MODIFIER_SWITCH_GR': TangleConstant.modifierSwitchRG,

  'MODIFIER_SWITCH_GB': TangleConstant.modifierSwitchGB,
  'MODIFIER_SWITCH_BG': TangleConstant.modifierSwitchGB,

  'MODIFIER_SWITCH_BR': TangleConstant.modifierSwitchBR,
  'MODIFIER_SWITCH_RB': TangleConstant.modifierSwitchBR,

  'true': TangleConstant.constantTrue,
  'false': TangleConstant.constantFalse,
};

final Map<TangleConstant, int> _constantsAsBytes = {
  TangleConstant.modifierSwitchNone: 0,
  TangleConstant.modifierSwitchRG: 1,
  TangleConstant.modifierSwitchGB: 2,
  TangleConstant.modifierSwitchBR: 3,
  TangleConstant.constantFalse: 0,
  TangleConstant.constantTrue: 1,
};

enum TangleConstant {
  modifierSwitchNone,
  modifierSwitchRG,
  modifierSwitchGB,
  modifierSwitchBR,
  constantTrue,
  constantFalse,
}
