import 'dart:typed_data';

import 'package:spectoda_utils/src/tangle_parser/enums/network_flag.dart';
import 'package:spectoda_utils/src/tangle_parser/enums/tangle_flag.dart';
import 'package:spectoda_utils/src/tangle_parser/enums/token_type.dart';
import 'package:spectoda_utils/src/tangle_parser/tangle_writer.dart';
import 'package:spectoda_utils/src/tangle_parser/tokenizer/tangle_token.dart';
import 'package:spectoda_utils/src/tangle_parser/tokenizer/tangle_tokenizer.dart';

class TangleCodeParser {
  final _tokenizer = TangleTokenizer();
  final _parser = TangleWriter();

  Uint8List toBigEndianBytes(int value) => _parser.toBigEndianBytes(value);

  Uint8List toBytes(String tangleCode) {
    final tokens = _tokenizer.tokenize(tangleCode);
    _writeTokens(tokens);
    _parser.fillFlag(TangleFlag.endOfTangleBytes);
    final int bytesCount = _parser.bufferLength;

    _parser.resetBuffer();
    _parser.fillNetworkFlag(NetworkFlag.flagTangleBytes);
    _parser.fillInt32BigEndian(bytesCount);
    _writeTokens(tokens);
    _parser.fillFlag(TangleFlag.endOfTangleBytes);
    return _parser.dumpBytes();
  }

  void _writeTokens(Iterable<TangleToken> tokens) {
    for (final TangleToken token in tokens) {
      _writeToken(token);
    }
  }

  void _writeToken(TangleToken tangleToken) {
    final String token = tangleToken.token;
    final TokenType type = tangleToken.type;
    switch(type) {
      case TokenType.comment:
        // skip
        break;
      case TokenType.htmlrgb:
        _parser.writeColor(token);
        break;
      case TokenType.infinity:
        _parser.writeInfinity(token);
        break;
      case TokenType.string:
        _parser.writeString(token);
        break;
      case TokenType.timestamp:
        _parser.writeTimestamp(token);
        break;
      case TokenType.label:
        _parser.writeLabel(token);
        break;
      case TokenType.char:
        _parser.writeChar(token);
        break;
      case TokenType.byte:
        _parser.writeByte(token);
        break;
      case TokenType.pixels:
        _parser.writePixels(token);
        break;
      case TokenType.percentage:
        _parser.writePercentage(token);
        break;
      case TokenType.float:
        throw Exception('"Naked" numbers are not permitted');
        //break;
      case TokenType.number:
        throw Exception('"Naked" numbers are not permitted');
        //break;
      case TokenType.arrow:
        // skip
        break;
      case TokenType.word:
        _parser.writeWord(token);
        break;
      case TokenType.whitespace:
        //skip
        break;
      case TokenType.punctuation:
        if (token == '}') {
          _parser.fillFlag(TangleFlag.endOfStatement);
        }
        break ;
      default:
        throw Exception('Unknown TokenType: "$type"');
    }
  }
}
