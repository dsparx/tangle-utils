import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:spectoda_utils/src/tangle_parser/enums/device_flag.dart';
import 'package:spectoda_utils/src/tangle_parser/enums/network_flag.dart';
import 'package:spectoda_utils/src/tangle_parser/tangle_writer.dart';

class TangleMetadataParser {
  final _parser = TangleWriter();

  Uint8List packetBytes({required int uuid, required int from, required int to, required Uint8List payload}) {
    _parser.fillInt32BigEndian(uuid);
    _parser.fillInt32BigEndian(from);
    _parser.fillInt32BigEndian(payload.length);
    return Uint8List.fromList([..._parser.dumpBytes(), ...payload.sublist(from, to)]);
  }

  Uint8List timeBytes({required int clock, required int timeline, bool isTimelinePaused = false}) {
    _parser.fillNetworkFlag(NetworkFlag.flagSetTimeline);
    _parser.fillInt32BigEndian(clock);
    _parser.fillInt32BigEndian(timeline);
    _parser.fillUint8(_getTimelineFlags(isTimelinePaused));
    return _parser.dumpBytes();
  }

  Uint8List colorEventBytes({required String label, required  Color color, required  int timeline, int deviceID = 255}) {
    _parser.fillNetworkFlag(NetworkFlag.flagEmitColorEvent); //TODO: emit LAZY event

    _parser.fillUint8(color.red);
    _parser.fillUint8(color.green);
    _parser.fillUint8(color.blue);

    _parser.writeLabel(label, withFlag: false);
    _parser.fillInt32BigEndian(timeline); //TODO: remove
    _parser.fillUint8(deviceID);
    return _parser.dumpBytes();
  }

  Uint8List percentageEventBytes({required String label, required double percentage, required int timeline, int deviceID = 255}) {
    _parser.fillNetworkFlag(NetworkFlag.flagEmitPercentageEvent); //TODO: emit LAZY event

    _parser.writePercentage('${(percentage * 100).floor()}%', withFlag: false);

    _parser.writeLabel(label, withFlag: false);
    _parser.fillInt32BigEndian(timeline); //TODO: remove
    _parser.fillUint8(deviceID);
    return _parser.dumpBytes();
  }

  Uint8List adoptRequestBytes({required int uuid, required Uint8List ownerSignature, required Uint8List ownerKey, required String deviceName, required int deviceID}) {
    _parser.fillDeviceFlag(DeviceFlag.flagAdoptRequest);
    _parser.fillInt32BigEndian(uuid);
    _parser.writeHash(ownerSignature);
    _parser.writeHash(ownerKey);
    _parser.writeString(deviceName, length: 16, noQuotes: true);
    _parser.fillUint8(deviceID);
    return _parser.dumpBytes();
  }

  Uint8List connectedPeersRequestBytes({required int uuid}) {
    _parser.fillDeviceFlag(DeviceFlag.flagConnectedPeersInfoRequest);
    _parser.fillInt32BigEndian(uuid);
    return _parser.dumpBytes();
  }

  Uint8List voltageRequestBytes({required int uuid, required int pin}) {
    _parser.fillDeviceFlag(DeviceFlag.flagVoltageOnPinRequest);
    _parser.fillInt32BigEndian(uuid);
    _parser.fillUint8(pin);
    return _parser.dumpBytes();
  }

  Uint8List networkBytes({required Uint8List bytes}) {
    _parser.fillNetworkFlag(NetworkFlag.deviceConfBytes);
    _parser.fillInt32BigEndian(bytes.length);
    return Uint8List.fromList([..._parser.dumpBytes(), ...bytes]);
  }

  Uint8List otaReset() {
    _parser.fillDeviceFlag(DeviceFlag.flagOtaReset);
    _parser.fillUint8(0x00);
    _parser.fillInt32BigEndian(0x00000000);
    return _parser.dumpBytes();
  }

  Uint8List otaBegin({required Uint8List firmware}) {
    _parser.fillDeviceFlag(DeviceFlag.flagOtaBegin);
    _parser.fillUint8(0x00);
    _parser.fillInt32BigEndian(firmware.length);
    return _parser.dumpBytes();
  }

  Uint8List otaWrite({required int written, required int from, required int to, required Uint8List firmware}) {
    _parser.fillDeviceFlag(DeviceFlag.flagOtaWrite);
    _parser.fillUint8(0x00);
    _parser.fillInt32BigEndian(written);
    return Uint8List.fromList([..._parser.dumpBytes(), ...firmware.sublist(from, to)]);
  }

  Uint8List otaEnd({required int written}) {
    _parser.fillDeviceFlag(DeviceFlag.flagOtaEnd);
    _parser.fillUint8(0x00);
    _parser.fillInt32BigEndian(written);
    return _parser.dumpBytes();
  }

  Uint8List rebootBytes() {
    _parser.fillNetworkFlag(NetworkFlag.deviceConfBytes);
    _parser.fillInt32BigEndian(1);
    _parser.fillDeviceFlag(DeviceFlag.flagDeviceReboot);
    return _parser.dumpBytes();
  }

  int _getTimelineFlags(bool timelinePaused) {
    // flags bits: [ Reserved,Reserved,Reserved,PausedFLag,Reserved,Reserved,Reserved,Reserved ]
    return int.parse(timelinePaused ? '00010000' : '00000000');
  }
}
