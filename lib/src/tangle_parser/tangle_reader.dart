import 'dart:typed_data';

class TangleReader {
  Uint8List data = Uint8List.fromList([]);
  int index = 0;

  void setDataToRead(List<int> bytes) {
    data = Uint8List.fromList(bytes);
    index = 0;
  }

  List<int> readBytes(int byteCount) {
    if (index + byteCount <= data.length) {
      final value = data.sublist(index, index + byteCount);
      forward(byteCount);
      return value;
    } else {
      throw Exception('Bytes read out of range');
    }
  }

  int readPercentage() {
    const int inMin = -2147483648;
    const int inMax = 2147483647;
    const int outMin = -100;
    const int outMax = 100;

    final int value = readUint32();

    final int result = (((value - inMin) * (outMax - outMin)) / (inMax - inMin) + outMin).toInt();

    return result.clamp(outMin, outMax);
  }

  int readFlag() => readUint8();

  int readUint8() => readValue(1, isUnsigned: true);

  int readInt32() => readValue(4, isUnsigned: false);

  int readUint16() => readValue(2, isUnsigned: true);

  int readUint32() => readValue(4, isUnsigned: true);

  int readValue(int byteCount, {required bool isUnsigned}) {
    try {
      final int val = peekValue(byteCount, isUnsigned: isUnsigned);
      forward(byteCount);
      return val;
    } catch (e) {
      throw Exception('Read out of range');
    }
  }

  int peekValue(int byteCount, {required bool isUnsigned}) {
    if (index + byteCount <= data.length) {
      int val = 0;
      for (int i = byteCount - 1; i >= 0; --i) {
        val <<= 8;
        val |= data.elementAt(index + i);
      }
      return val; // Equivalent to tangle-js version: unsigned ? value >>> 0 : value;
    } else {
      throw Exception('Peeked out of range');
    }
  }

  void forward(int byteCount) {
    if (index + byteCount <= data.length)  {
      index += byteCount;
    } else {
      index = data.length;
    }
  }
}
