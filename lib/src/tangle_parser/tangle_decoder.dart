import 'package:spectoda_utils/src/models/battery.dart';
import 'package:spectoda_utils/src/models/battery_event.dart';
import 'package:spectoda_utils/src/tangle_parser/enums/device_flag.dart';
import 'package:spectoda_utils/src/tangle_parser/enums/network_flag.dart';
import 'package:spectoda_utils/src/tangle_parser/tangle_reader.dart';

class TangleDecoder {
  final reader = TangleReader();
  final deviceConverter = DeviceFlagConverter();
  final networkConverter = NetworkFlagConverter();

  List<int> adoptResponse({required List<int> response, required int requestUUID}) {
    if (response.isEmpty) {
      throw Exception('Response is empty');
    }
    reader.setDataToRead(response);

    final int responseFlag = reader.readFlag();
    if (responseFlag != deviceConverter.toByte(DeviceFlag.flagAdoptResponse)) {
      throw Exception('Invalid response');
    }

    final int responseUUID = reader.readUint32();
    if (responseUUID != requestUUID) {
      throw Exception('Invalid response');
    }

    final int errorCode = reader.readUint8();
    if (errorCode == 0) {
      final deviceMac = reader.readBytes(6);
      return deviceMac;
    } else {
      throw Exception('Adoption failed');
    }
  }

  NetworkFlag networkEventType({required List<int> response}) {
    reader.setDataToRead(response);
    final responseFlag = reader.readFlag();
    return networkConverter.fromByte(responseFlag);
  }

  Battery batteryEvent({required List<int> response}) {
    if (response.isEmpty) {
      throw Exception('Response is empty');
    }
    reader.setDataToRead(response);

    final int responseFlag = reader.readFlag();
    if (responseFlag != networkConverter.toByte(NetworkFlag.flagEmitPercentageEvent)) {
      throw Exception('Invalid response');
    }

    final percentage = reader.readPercentage();
    final label = String.fromCharCodes(reader.readBytes(5));
    final timestamp = reader.readInt32();
    final deviceID = reader.readUint8();

    final ChargingEvent chargingEvent;
    if (label == 'CH_DO') {
      chargingEvent = ChargingEvent.done;
    } else if (label == 'CH_IN') {
      chargingEvent = ChargingEvent.inProgress;
    } else if (label == 'CH_UN') {
      chargingEvent = ChargingEvent.unplugged;
    } else if (label == 'VOLTA') {
      chargingEvent = ChargingEvent.legacyVolta;
    } else {
      throw UnimplementedError('ChargingEvent has a unknown value "$label"');
    }

    return Battery(
      percentage: percentage,
      chargingEvent: chargingEvent,
      deviceID: deviceID,
      timestamp: Duration(milliseconds: timestamp),
    );
  }

  List<String> connectedPeersInfoResponse({required List<int> response, required int requestUUID}) {
    if (response.isEmpty) {
      throw Exception('Response is empty');
    }
    reader.setDataToRead(response);

    final int responseFlag = reader.readFlag();
    if (responseFlag != deviceConverter.toByte(DeviceFlag.flagConnectedPeersInfoResponse)) {
      throw Exception('Invalid response');
    }

    final int responseUUID = reader.readUint32();
    if (responseUUID != requestUUID) {
      throw Exception('Invalid response');
    }

    final int errorCode = reader.readUint8();
    if (errorCode == 0) {
      final List<String> peers = [];
      final count = reader.readUint16();
      for (int i = 0; i < count; ++i) {
        final mac = reader.readBytes(6);
        peers.add(String.fromCharCodes(mac));
      }
      return peers;
    } else {
      throw Exception('Requesting connected peers info failed!');
    }
  }

  String peerConnectedEvent({required List<int> response}) {
    if (response.isEmpty) {
      throw Exception('Response is empty');
    }
    reader.setDataToRead(response);

    final int responseFlag = reader.readFlag();
    if (responseFlag != networkConverter.toByte(NetworkFlag.flagPeerConnected)) {
      throw Exception('Invalid response');
    }

    final List<int> mac = reader.readBytes(6);
    return String.fromCharCodes(mac);
  }

  String peerDisconnectedEvent({required List<int> response}) {
    if (response.isEmpty) {
      throw Exception('Response is empty');
    }
    reader.setDataToRead(response);

    final int responseFlag = reader.readFlag();
    if (responseFlag != networkConverter.toByte(NetworkFlag.flagPeerDisconnected)) {
      throw Exception('Invalid response');
    }

    final List<int> mac = reader.readBytes(6);
    return String.fromCharCodes(mac);
  }
}
