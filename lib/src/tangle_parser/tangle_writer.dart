import 'dart:math';
import 'dart:typed_data';

import 'package:spectoda_utils/src/tangle_parser/enums/device_flag.dart';
import 'package:spectoda_utils/src/tangle_parser/enums/network_flag.dart';
import 'package:spectoda_utils/src/tangle_parser/enums/tangle_constant.dart';
import 'package:spectoda_utils/src/tangle_parser/enums/tangle_flag.dart';

class TangleWriter {
  final _flagConverter = TangleFlagConverter();
  final _networkConverter = NetworkFlagConverter();
  final _deviceConverter = DeviceFlagConverter();
  final _constantConverter = TangleConstantConverter();

  List<int> _buffer = [];

  Uint8List toBigEndianBytes(int value) => Uint8List.fromList([
    value,
    value >> 8,
    value >> 16,
    value >> 24,
  ]);

  Uint8List dumpBytes() {
    final bytes = Uint8List.fromList(_buffer);
    resetBuffer();
    return bytes;
  }

  void resetBuffer() => _buffer = [];

  int get bufferLength => _buffer.length;

  void writeByte(String byte) {
    fillUint8(int.parse(byte));
  }

  void writeChar(String firstMatch) {
    fillUint8(firstMatch.codeUnitAt(1));
  }

  void writeString(String str, {int length = 8, bool noQuotes = false}) {
    String withoutQuotes = str.substring(1, str.length - 1);
    if (noQuotes) {
      withoutQuotes = str;
    }

    final upperBound = min(length, withoutQuotes.length);
    final remaining = length - upperBound;
    for (int i = 0; i < upperBound; ++i) {
      fillUint8(withoutQuotes.codeUnitAt(i));
    }
    for (int i = 0; i < remaining; ++i) {
      fillUint8(0);
    }
  }

  void writeInfinity(String str) {
    if (str == 'Infinity' || str == '+Infinity') {
      fillFlag(TangleFlag.timestampMax);
    } else if (str == '-Infinity') {
      fillFlag(TangleFlag.timestampMin);
    } else {
      throw Exception('Failed to write Infinity from "$str"');
    }
  }

  void writeTimestamp(String str) {
    final int tics = timestampToTics(str);
    if (tics == 0) {
      fillFlag(TangleFlag.timestampZero);
    } else {
      fillFlag(TangleFlag.timestamp);
      fillInt32BigEndian(tics);
    }
  }

  void writeColor(String str) {
    final int red = int.parse(str.substring(1, 3), radix: 16);
    final int green = int.parse(str.substring(3, 5), radix: 16);
    final int blue = int.parse(str.substring(5), radix: 16);
    if (red == 0 && green == 0 && blue == 0) {
      fillFlag(TangleFlag.colorBlack);
    } else if (red == 255 && green == 255 && blue == 255) {
      fillFlag(TangleFlag.colorWhite);
    } else {
      fillFlag(TangleFlag.color);
      fillUint8(red);
      fillUint8(green);
      fillUint8(blue);
    }
  }

  void writePercentage(String str, {bool withFlag = true}) {
    const int inMin = -100;
    const int inMax = 100;
    const int outMin = -2147483648;
    const int outMax = 2147483647;

    final withoutSymbol = str.substring(0, str.length - 1);
    final int percent = int.parse(withoutSymbol);
    final int percentLimited = percent.clamp(-100, 100);

    final int result = (((percentLimited - inMin) * (outMax - outMin)) / (inMax - inMin) + outMin).toInt();

    if (withFlag) fillFlag(TangleFlag.percentage);
    final int resultLimited = result.clamp(outMin, outMax);
    fillInt32BigEndian(resultLimited);
  }

  void writeLabel(String label, {bool withFlag = true}) {
    final String withoutSymbol = label.substring(1);
    final upperBound = min(5, withoutSymbol.length);
    final remaining = 5 - upperBound;

    if (withFlag) fillFlag(TangleFlag.label);
    for (int i = 0; i < upperBound; ++i) {
      fillUint8(withoutSymbol.codeUnitAt(i));
    }
    for (int i = 0; i < remaining; ++i) {
      fillUint8(0);
    }
  }

  void writePixels(String str) {
    final withoutSymbol = str.substring(0, str.length - 2);
    fillFlag(TangleFlag.pixels);
    fillInt16(int.parse(withoutSymbol));
  }

  void writeWord(String word) {
    int byte;
    try {
      byte = _flagConverter.fromWordToByte(word);
      fillUint8(byte);
    } on Exception {
      try {
        byte = _constantConverter.fromWordToByte(word);
        fillUint8(byte);
      } on Exception {
        throw Exception('Unknown word "$word"');
      }
    }
  }

  void writeHash(Uint8List bytes, {int length = 16}) {
    assert(bytes.length == length);

    for (int i = 0; i < length; ++i) {
      fillUint8(bytes[i]);
    }
  }

  void fillInt32BigEndian(int value) {
    final bytes = toBigEndianBytes(value);
    fillUint8(bytes[0]);
    fillUint8(bytes[1]);
    fillUint8(bytes[2]);
    fillUint8(bytes[3]);
  }

  void fillInt16(int value) {
    final bytes = toBigEndianBytes(value);
    fillUint8(bytes[0]);
    fillUint8(bytes[1]);
  }

  void fillFlag(TangleFlag flag) {
    fillUint8(_flagConverter.toByte(flag));
  }

  void fillNetworkFlag(NetworkFlag flag) {
    fillUint8(_networkConverter.toByte(flag));
  }

  void fillDeviceFlag(DeviceFlag flag) {
    fillUint8(_deviceConverter.toByte(flag));
  }

  void fillUint8(int value) {
    _buffer.add(value);
  }

  int timestampToTics(String timestamp) {
    final fragmentRegex = RegExp('([+-]?[0-9]*[.]?[0-9]+)([dhmst])');

    double totalTics = 0;
    String remainingTimestamp = timestamp;
    while (remainingTimestamp.isNotEmpty) {
      final Match fragment = fragmentRegex.allMatches(timestamp).first;

      if (fragment.groupCount != 2) {
        throw Exception('Timestamp fragment has invalid format');
      }

      final String value = fragment.group(0)!; // gets "-1.4d" from "-1.4d"
      final String unit = fragment.group(2)!; // gets "d" from "-1.4d"
      final double number = double.parse(fragment.group(1)!); // gets "-1.4" from "-1.4d"

      totalTics += toTics(number, unit);

      remainingTimestamp = remainingTimestamp.substring(value.length);
    }
    return totalTics.round();
  }

  double toTics(double number, String unit) {
    switch (unit) {
      case 'd':
        return number * 86400000;
      case 'h':
        return number * 3600000;
      case 'm':
        return number * 60000;
      case 's':
        return number * 1000;
      case 't':
        return number;
      default:
        throw Exception('Unknown timestamp unit "$unit"');
    }
  }
}
