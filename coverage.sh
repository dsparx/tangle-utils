#!/bin/bash

##
## Runs all Flutter tests with coverage analysis mode, generating all necessary
## test coverage files.
##
## Note: The generated html files can be displayed in any browser by opening the coverage/index.html
##
## Inspired by: http://blog.wafrat.com/test-coverage-in-dart-and-flutter
##

# Install the test coverage tool if not installed
lcov -v
if [ $? != 0 ]
then
## If apt tool doesnt work, try brew tool (for MAC OS)
    sudo apt install lcov -y
    if [ $? != 0 ]
    then
      brew install lcov
    fi
fi

# Generate the basic lcov.info file
flutter test --coverage
RESULT=$?

# From the basic file generate the html report files
genhtml coverage/lcov.info -o coverage

# Add a README.md to the coverage directory
echo "Open index.html in any browser to display the lcov (test coverage) report" > coverage/README.md

# Print to the console where you can find the more detailed report
echo ""
echo "NOTE: Open coverage/index.html in any browser to display the detailed lcov (test coverage) report"

exit $RESULT
